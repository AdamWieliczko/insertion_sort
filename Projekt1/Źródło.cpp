#include <iostream>

using namespace std;

class Sorting
{
public:

	int array[10] = { 4, 5, 1, 2, 6, 3, 8, 7, 10, 9 };

	void InsertionSort()
	{
		for (int i, element, j = 2; j < 10; j++)
		{
			element = array[j];
			i = j - 1;
			while (i >= 0 && array[i] > element)
			{
				array[i + 1] = array[i];
				i = i - 1;
			}
			array[i + 1] = element;
		}
	}

	void printArray()
	{
		for (int i = 0; i < 10; i++)
		{
			cout << array[i] << " ";
		}

		cout << endl;
	}

	void isSortingCorrect()
	{
		bool isItCorrect = true;

		for (int i = 0; i < 9; i++)
		{
			if (array[i] > array[i + 1])
			{
				isItCorrect = false;
			}
		}

		if (isItCorrect == true)
		{
			cout << "Sortowanie zakonczone pomyslnie" << endl;
		}
		else
		{
			cout << "Sortowanie zakonczone niepoprawnie" << endl;
		}
	}

};



int main()
{
	Sorting ok;
	ok.InsertionSort();
	ok.printArray();
	ok.isSortingCorrect();
	return 0;
}
